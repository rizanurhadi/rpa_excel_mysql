"""
check folder
open excel
click update data
wait to finish
transform data
"""
import os
import time
from automagica import OpenFile, ClickOnPosition, OpenNotepad, Type, PressHotkey, ExcelReadRowCol
from openpyxl import Workbook, load_workbook
import ceic_exim_series_trade_statistic_mdl
import ceic_exim_mst_trade_statistics_mdl
import ceic_exim_mst_mdl
import ceic_perkembangan_eko_mdl

def ver01():
    """
    get filename
    """
    folder_excel = r"E:\pyproject\rpaproject\sample_excel"
    for filename in os.listdir(folder_excel):
        if filename.endswith(".xlsx") : 
            print(os.path.join(folder_excel, filename))

def ver02():
    """
    1. open filename
    2. click something
    3. wait
    """
    folder_excel = r"E:\pyproject\rpaproject\sample_excel\ID DCExports fob Local Currency.xlsx"
    OpenFile(path=folder_excel)
    time.sleep(5)
    ClickOnPosition(1160,78)
    time.sleep(5)
    ClickOnPosition(1350,170)
    time.sleep(5)
    #click data
    ClickOnPosition(370,50)
    #click get data
    time.sleep(5)
    #click data
    ClickOnPosition(30,90)
    #opennotepad and write waiting data to load
    OpenNotepad()
    time.sleep(5)
    Type('tunggu data load dari ceicdata', interval_seconds=0.01)
    time.sleep(5)

    PressHotkey("alt","tab")
    time.sleep(5)
    ClickOnPosition(1342,16)

def ver03():
    """
    using python to load data from excel into dbase
    """
    file_excel = r"E:\pyproject\rpaproject\sample_excel\ID DCExports fob Local Currency.xlsx"
    #check max row and col
    wb = load_workbook(file_excel,read_only=True)
    sheet = wb.worksheets[0]
    row_count = sheet.max_row
    column_count = sheet.max_column
    print("--------")
    print(row_count)
    print(column_count)
    print("--------")
    series_id = sheet.cell(row=8, column=2).internal_value
    for iterator in range (27,row_count):
        tanggal = sheet.cell(row=iterator, column=1).value
        data_series = sheet.cell(row=iterator, column=2).internal_value
        myyield = [str(tanggal), str(series_id), str(data_series)]
        print(str(series_id) + '|' + str(tanggal) + '|' + str(data_series)  )
        #ceic_exim_series_trade_statistic_mdl.upsert('ceic',myyield)
        ceic_perkembangan_eko_mdl.upsert('ceic',myyield)

def ver03_withalotofcol():
    """
    using python to load data from excel into dbase series
    """
    file_excel = r"E:\pyproject\rpaproject\sample_excel\ID DCExports fob Local Currency.xlsx"
    #check max row and col
    wb = load_workbook(file_excel,read_only=True)
    sheet = wb.worksheets[0]
    row_count = sheet.max_row
    column_count = sheet.max_column
    for iterator_col in range (2,column_count+1):
        series_id = sheet.cell(row=8, column=iterator_col).internal_value
        for iterator_row in range (27,row_count+1):
            tanggal = sheet.cell(row=iterator_row, column=1).value
            data_series = sheet.cell(row=iterator_row, column=iterator_col).internal_value
            myyield = [str(tanggal), str(series_id), str(data_series)]
            ceic_perkembangan_eko_mdl.upsert('ceic',myyield)

def ver04():
    """
    this should be handle 
    m_2_summary ekspor impor neraca perdagangan ceic 
    sheet yg digunakan 'data'

    exim_mst_trade_statistic (type_data1,type_data2,type_data3,type_data4,type_data5)
    row1 col2[not 1] . dipisah dengan ':'

    exim_mst_trade_statistic (country, frequency, unit, source, status,
    series_id, sr_code, function_information , first_obs_date, last_obs_date
    last_update_time, remarks, suggestion, mean, variance,
    standard_deviation, skewness, kurtosis, coefficient_variantion, min,
    max, median, no_of_obs)
    row2 ~ 24

    exim series trade statistics (tanggal, data_series, series id, insert_date)
    start at row 25
    """
    file_excel = r"E:\pyproject\rpaproject\sample_excel\ID DCExports fob Local Currency.xlsx"
    #check max row and col
    wb = load_workbook(file_excel,read_only=True)
    sheet = wb.worksheets[0]
    row_count = sheet.max_row
    column_count = sheet.max_column
    type_data = sheet.cell(row=1, column=2).internal_value
    type_data_split = type_data.split(':')
    myyield = {
        'type_data1':'',
        'type_data2':'',
        'type_data3':'',
        'type_data4':'',
        'type_data5':'',
    }
    iterasi = 1
    for myrow1data in type_data_split :
        myyield['type_data' + str(iterasi)] = myrow1data
        iterasi +=1
    print(myyield)
    
""" for iterator_col in range (2,max_column):
        sheet.cell(row=1, column=iterator_col).internal_value """

def ver04_withlotofcol():
    file_excel = r"E:\pyproject\rpaproject\sample_excel\ID DCExports fob Local Currency.xlsx"
    #check max row and col
    wb = load_workbook(file_excel,read_only=True)
    sheet = wb.worksheets[0]
    row_count = sheet.max_row
    column_count = sheet.max_column
    for iterator_col in range (2,column_count+1):
        type_data = sheet.cell(row=1, column=iterator_col).internal_value
        type_data_split = type_data.split(':')
        myyield = {
            'type_data1':'',
            'type_data2':'',
            'type_data3':'',
            'type_data4':'',
            'type_data5':'',
        }
        iterasi = 1
        for myrow1data in type_data_split :
            myyield['type_data' + str(iterasi)] = myrow1data
            iterasi +=1
        print(myyield)

def ver05():
    """
    this should be handle for row 2 ~ 14
    """
    file_excel = r"E:\pyproject\rpaproject\sample_excel\ID DCExports fob Local Currency.xlsx"
    #check max row and col
    wb = load_workbook(file_excel,read_only=True)
    sheet = wb.worksheets[0]
    row_count = sheet.max_row
    column_count = sheet.max_column
    myyield = {
        'series_id':''
    }
    for iterator_col in range (2,column_count+1):
        for iterator_row in range (2,26):
            mycolname = sheet.cell(row=iterator_row, column=1).internal_value
            print(mycolname.strip().lower().replace(' ','_'))
            mycolval =  sheet.cell(row=iterator_row, column=iterator_col).internal_value
            #print (mycolval)
            myyield[mycolname.strip().lower().replace(' ','_')] = mycolval
        print('-----------')
        print(myyield)

def ver06():
    """
    this should be handle for row 1 ~ 24 /26
    """
    print("load data for mst with series id")
    file_excel = r"E:\pyproject\rpaproject\sample_excel\ID DCExports fob Local Currency.xlsx"
    #check max row and col
    wb = load_workbook(file_excel,read_only=True)
    sheet = wb.worksheets[0]
    row_count = sheet.max_row
    column_count = sheet.max_column
    myyield = {
        'series_id':'',
        'pdb_pdrb_id':'',
        'type_data1':'',
        'type_data2':'',
        'type_data3':'',
        'type_data4':'',
        'type_data5':'',
        'type_data6':'',
        'type_data7':'',
    }

    for iterator_col in range (2,column_count+1):
        type_data = sheet.cell(row=1, column=iterator_col).internal_value
        type_data_split = type_data.split(':')
        # load tipedata
        iterasi = 1
        mystr = mystr2 = ''
        #myyield['type_data1']=myyield['type_data2']=myyield['type_data3']=myyield['type_data4']=myyield['type_data5']=''
        for myrow1data in type_data_split :
            myyield['type_data' + str(iterasi)] = myrow1data
            mystr = mystr + "myyield['type_data"+ str(iterasi) + "'],"
            mystr2 = mystr2 + 'type_data' + str(iterasi) + ','
            iterasi +=1
        for iterator_row in range (2,27):
            mycolname = sheet.cell(row=iterator_row, column=1).internal_value
            #print(mycolname.strip().lower().replace(' ','_'))
            
            mycolval =  sheet.cell(row=iterator_row, column=iterator_col).internal_value
            if mycolval :
                if sheet.cell(row=iterator_row, column=iterator_col).is_date :
                    mycolval =  str(sheet.cell(row=iterator_row, column=iterator_col).value)
            else :
                mycolval = 'test'
            
            #print (mycolval)
            mystr = mystr + "myyield['"+mycolname.strip().lower().replace(' ','_').replace('.','')+"'],"
            mystr2 = mystr2 + mycolname.strip().lower().replace(' ','_').replace('.','') + ','
            myyield[mycolname.strip().lower().replace(' ','_').replace('.','')] = mycolval
            #myyield[mycolname.strip().lower().replace(' ','_').replace('.','')] = ''
        myyield2 = [myyield['series_id'],'',myyield['type_data1'],myyield['type_data2'],myyield['type_data3'],myyield['type_data4'],myyield['type_data5'],'','',myyield['region'],myyield['subnational'],myyield['frequency'],myyield['unit'],myyield['source'],myyield['status'],myyield['sr_code'],myyield['mnemonic'],myyield['function_information'],myyield['first_obs_date'],myyield['last_obs_date'],myyield['last_update_time'],myyield['series_remarks'],myyield['suggestions'],myyield['mean'],myyield['variance'],myyield['standard_deviation'],myyield['skewness'],myyield['kurtosis'],myyield['coefficient_variation'],myyield['min'],myyield['max'],myyield['median'],myyield['no_of_obs']]
        ceic_exim_mst_mdl.upsert('ceic','exim_mst_trade_statistic',myyield2)
        #print('-----------')
        #print(myyield)
        #print('-----------')
        #print(mystr)
        #print('-----------')
        #print(mystr2)
        

def ver07():
    mystr =''
    for row in range(0,30) :
        #mystr = mystr + 'row['+str(row)+'],'
        mystr = mystr + " %s, "
    print(mystr)

if __name__ == "__main__":
    print("running test 03")
    #ver02()
    #ver03()
    #ver03_withalotofcol()
    #ver04()
    #ver04_withlotofcol()
    #ver05()
    ver06()
    #ver07()