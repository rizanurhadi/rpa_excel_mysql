#!/usr/bin/python
# -*- coding: utf-8 -*-
import mysql.connector
from configdbmy import config
import csv
import time
import os

def upsert(url,dbname, row):
    timestr = time.strftime("%Y-%m-%d %H:%M:%S")
    #fieldnames = ['tanggal','series_id','data_series', 'data_value','insert_date']
    rowid = get_one(dbname, row[0],row[1])
    if rowid :
        update(rowid,url,timestr,dbname, row[0],row[1],row[2],timestr)
    else :
        rowid = insert(url,timestr,dbname, row[0],row[1],row[2],timestr)
    print("saved data ke %s with %s , %s , %s", (dbname, rowid,row[0],row[1]))

def insert(url,crdate,dbname, tanggal,series_id,data_series,insert_date):
    mysqldb = mysql.connector
    """ Insert with cek if exists"""
    sql = """ 
    INSERT INTO {dbname} (tanggal,series_id,data_series,insert_date) 
    VALUES ( %s, %s,%s,%s);""".format(dbname=dbname)
    conn = None
    vendor_id = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = mysqldb.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the INSERT statement
        cur.execute(sql, (tanggal,series_id,data_series,insert_date))
        # get the generated id back
        #vendor_id = cur.fetchone()[0]
        vendor_id = cur.lastrowid
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, mysqldb.DatabaseError) as error:
        print(error)
        print('insert error')
    finally:
        if conn is not None:
            conn.close()
 
    return vendor_id

def update(id,url,crdate,dbname, tanggal,series_id,data_series,insert_date):
    mysqldb = mysql.connector
    """ Insert with cek if exists"""
    sql = """ 
    UPDATE {dbname} 
    SET tanggal=%s, series_id=%s, data_series=%s, insert_date=%s
    WHERE id=%s
    ;""".format(dbname=dbname)
    conn = None
    vendor_id = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = mysqldb.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the INSERT statement
        cur.execute(sql, (tanggal,series_id,data_series,insert_date,id))
        # get the generated id back
        #vendor_id = cur.fetchone()[0]
        vendor_id = cur.lastrowid
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, mysqldb.DatabaseError) as error:
        print(error)
        print('update error')
    finally:
        if conn is not None:
            conn.close()
 
    return vendor_id

def get_one(dbname, tanggal,series_id):
    mysqldb = mysql.connector
    """ query data from the vendors table """
    conn = None
    row = None
    try:
        params = config()
        conn = mysqldb.connect(**params)
        cur = conn.cursor()
        sql ="SELECT id FROM "+dbname+" WHERE tanggal = %s and series_id = %s LIMIT 1"
        cur.execute(sql,(tanggal,series_id))
        rowid = cur.fetchone()
        if rowid :
            row = rowid[0]
        #print(rowid)
        cur.close()
    except (Exception, mysqldb.DatabaseError) as error:
        print(error)
        print('get one error')
    finally:
        if conn is not None:
            conn.close()
    return row

def connect():
    """ Connect to the PostgreSQL database server """
    mysqldb = mysql.connector
    conn = None
    try:
        # read connection parameters
        params = config()
 
        # connect to the PostgreSQL server
        print('Connecting to the Mysql database...')
        conn = mysqldb.connect(**params)
      
        # create a cursor
        cur = conn.cursor()
        
   # execute a statement
        print('Mysql database version:')
        cur.execute('SELECT version()')
 
        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)
       
       # close the communication with the PostgreSQL
        cur.close()
    except (Exception, mysqldb.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

#if __name__ == '__main__':
#    connect()