import mysql.connector
from configdbmy import config
import csv
import time
import os

def upsert(url,dbname,row):
    timestr = time.strftime("%Y-%m-%d %H:%M:%S")
    #fieldnames = [series_id , pdb_pdrb_id, type_data1, type_data2, type_data3, type_data4 , type_data5, type_data6 , type_data7
    # , region , subnational , frequency , unit , source , status , sr_code , mnemonic 
    # , function_information , first_obs_date , last_obs_date , last_update_time
    #  , series_remarks , suggestions , mean , variance , standard_deviation 
    # , skewness , kurtosis , coefficient_variation , min , max , median ,
    #  no_of_obs ,'insert_date']
    rowid = get_one(dbname, row[0])
    if rowid :
        update(rowid,url,timestr,dbname,row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13],row[14],row[15],row[16],row[17],row[18],row[19],row[20],row[21],row[22],row[23],row[24],row[25],row[26],row[27],row[28],row[29],row[30],row[31],row[32],timestr)
    else :
        rowid = insert(url,timestr,dbname,row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13],row[14],row[15],row[16],row[17],row[18],row[19],row[20],row[21],row[22],row[23],row[24],row[25],row[26],row[27],row[28],row[29],row[30],row[31],row[32],timestr)
    print("saved data into  %s with id %s , %s , %s", (dbname, rowid,row[0],row[1]))

def insert(url,crdate,dbname,series_id ,pdb_pdrb_id, type_data1, type_data2, type_data3  , type_data4 , type_data5 , type_data6 , type_data7 ,  region , subnational , frequency , unit , source , status , sr_code , mnemonic , function_information , first_obs_date , last_obs_date , last_update_time , series_remarks , suggestions , mean , variance , standard_deviation , skewness , kurtosis , coefficient_variation , min , max , median , no_of_obs ,insert_date):
    mysqldb = mysql.connector
    sql = """ 
    INSERT INTO {dbname} (series_id ,pdb_pdrb_id, type_data1, type_data2, type_data3  , type_data4 , type_data5 ,type_data6 , type_data7 , region , subnational , frequency , unit , source , status , sr_code , mnemonic , function_information , first_obs_date , last_obs_date , last_update_time , series_remarks , suggestions , mean , variance , standard_deviation , skewness , kurtosis , coefficient_variation , min , max , median , no_of_obs ,insert_date) 
    VALUES (  %s,  %s,  %s,%s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  
%s,  %s,  %s,  %s,  %s ,%s);""".format(dbname=dbname)
    conn = None
    vendor_id = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = mysqldb.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the INSERT statement
        cur.execute(sql, ( series_id ,pdb_pdrb_id, type_data1, type_data2, type_data3  , type_data4 , type_data5,type_data6 , type_data7 , region , subnational , frequency , unit , source , status , sr_code , mnemonic , function_information , first_obs_date , last_obs_date , last_update_time , series_remarks , suggestions , mean , variance , standard_deviation , skewness , kurtosis , coefficient_variation , min , max , median , no_of_obs,insert_date))
        # get the generated id back
        #vendor_id = cur.fetchone()[0]
        vendor_id = cur.lastrowid
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, mysqldb.DatabaseError) as error:
        print(error)
        print('insert error')
    finally:
        if conn is not None:
            conn.close()
    return vendor_id

def update(id,url,crdate,dbname,series_id ,pdb_pdrb_id, type_data1, type_data2, type_data3  , type_data4 , type_data5,type_data6 , type_data7 , region , subnational , frequency , unit , source , status , sr_code , mnemonic , function_information , first_obs_date , last_obs_date , last_update_time , series_remarks , suggestions , mean , variance , standard_deviation , skewness , kurtosis , coefficient_variation , min , max , median , no_of_obs,insert_date):
    mysqldb = mysql.connector
    """ Insert with cek if exists"""
    sql = """ 
    UPDATE {dbname}
    SET series_id =%s,pdb_pdrb_id=%s, type_data1=%s, type_data2=%s, type_data3  =%s, type_data4 =%s, type_data5 =%s,type_data6 =%s,type_data7 =%s, region =%s, subnational =%s, frequency =%s, unit =%s, source =%s, status =%s, sr_code =%s, mnemonic =%s, function_information =%s, first_obs_date =%s, last_obs_date =%s, last_update_time =%s, series_remarks =%s, suggestions =%s, mean =%s, variance =%s, standard_deviation =%s, skewness =%s, kurtosis =%s, coefficient_variation =%s, min =%s, max =%s, median =%s, no_of_obs =%s, insert_date=%s
    WHERE id=%s
    ;""".format(dbname=dbname)
    conn = None
    vendor_id = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = mysqldb.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the INSERT statement
        cur.execute(sql, ( series_id ,pdb_pdrb_id, type_data1, type_data2, type_data3  , type_data4 , type_data5,type_data6 , type_data7 , region , subnational , frequency , unit , source , status , sr_code , mnemonic , function_information , first_obs_date , last_obs_date , last_update_time , series_remarks , suggestions , mean , variance , standard_deviation , skewness , kurtosis , coefficient_variation , min , max , median , no_of_obs,insert_date,id))
        # get the generated id back
        #vendor_id = cur.fetchone()[0]
        vendor_id = cur.lastrowid
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, mysqldb.DatabaseError) as error:
        print(error)
        print('update error')
    finally:
        if conn is not None:
            conn.close()
    return vendor_id

def get_one(dbname, series_id):
    mysqldb = mysql.connector
    """ query data from the vendors table """
    conn = None
    row = None
    try:
        params = config()
        conn = mysqldb.connect(**params)
        cur = conn.cursor()
        sql ="SELECT id FROM "+dbname+" WHERE series_id = %s LIMIT 1"
        #print('test')
        #print(sql % (dbname, series_id,))
        cur.execute(sql,( series_id,))
        rowid = cur.fetchone()
        if rowid :
            row = rowid[0]
        #print(rowid)
        cur.close()
    except (Exception, mysqldb.DatabaseError) as error:
        print(error)
        print('get one error')
    finally:
        if conn is not None:
            conn.close()
    return row